package com.epam.controller;

import com.epam.model.Order;
import com.epam.model.enums.PizzaTypes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class PizzaOrderServlet extends HttpServlet {
    private static Logger logger = LogManager.getLogger(PizzaOrderServlet.class);
    private static Map<Integer, Order> orders = new HashMap<>();

    @Override
    public void init() throws ServletException {
        logger.info("Servlet " + this.getServletName() + "has started");
        Order first = new Order(PizzaTypes.PEPERONI);
        Order second = new Order(PizzaTypes.CHEESE);
        Order third = new Order(PizzaTypes.CARBONARA);
        Order forth = new Order(PizzaTypes.VEGAN);

        orders.put(first.getId(), first);
        orders.put(second.getId(), second);
        orders.put(third.getId(), third);
        orders.put(forth.getId(), forth);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info(this.getServletName() + "in doGet");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<p><a href='orders'>REFRESH</a></p>");

        out.println("<h2>Orders</h2>");
        for (Order order : orders.values()) {
            out.println("<p>" + order + "</p>");
        }

        out.println("<form action='orders' method='POST'>\n" +
                "Pizza: <input type='text' name='pizza-name'>\n" +
                "<button type='submit'>Make Order</button>\n" +
                "</form>");

        out.println("<form>\n" +
                "<p><b>DELETE ELEMENT</b></p>\n" +
                "<p>Order id: <input type='text' name='user_id'>\n" +
                "<input type='button' onClick='remote(this.form.order_id.value)' name='ok' value='Delete order'>\n" +
                "</p>\n" +
                "</form>");

        out.println("<script type='text/javascript'>\n" +
                " function remove(id) {fetch('orders/' +id, {method: 'DELETE'});}\n" +
                "</script>");

        out.println("<p>Request URI:" + req.getRequestURI() + "</p>");
        out.println("<p>Method: " + req.getMethod() + "</p>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        logger.info("in doPost");
        String newPizzaName = req.getParameter("pizza_name");
        Order order = new Order(PizzaTypes.valueOf(newPizzaName));
        orders.put(order.getId(), order);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("in doDelete");
        String id = req.getRequestURI();
        logger.info("URI=" + id);
        id = id.replace("/task16_servlets/orders", "");
        logger.info("id=" + id);
        orders.remove(Integer.parseInt(id));
    }

    @Override
    public void destroy(){
        logger.info("Servlet " + this.getServletName() + " has stopped");
    }
}
