package com.epam.model;

import com.epam.model.enums.PizzaTypes;

public class Order {

    private static int unique = 1;
    private int id;
    private PizzaTypes pizzaTypes;

    public Order(PizzaTypes pizzaTypes) {
        this.pizzaTypes = pizzaTypes;
        id = unique;
        unique++;
    }

    public int getId() {
        return id;
    }

    public PizzaTypes getPizzaTypes() {
        return pizzaTypes;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", pizza=" + pizzaTypes +
                "price=" + pizzaTypes.getPrice() +
                '}';
    }
}

